set export
DIST := "dist/"

test:
    pytest

pypi: clean build_wheel upload_wheel

clean:
    rm -rf $DIST

build_wheel:
    python3 -m build --outdir $DIST

upload_wheel:
    python3 -m twine upload $DIST/*
